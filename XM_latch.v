module XM_latch(instr_out, alu_out, out_B, instr_in, alu_in, in_B, clock, enable, reset);
    input [31:0] instr_in, alu_in, in_B;
    input clock, enable, reset;

    output[31:0] instr_out, alu_out, out_B;

    registers32 write_instr_out(instr_out, instr_in, clock, enable, reset);
    registers32 write_alu_out(alu_out, alu_in, clock, enable, reset);
    registers32 write_out_B(out_B, in_B, clock, enable, reset);

endmodule