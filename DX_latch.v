module DX_latch(instr_out, PC_out, out_A, out_B, instr_in, PC_in, in_A, in_B, clock, enable, reset);
    input [31:0] instr_in, PC_in, in_A, in_B;
    input clock, enable, reset;

    output[31:0] instr_out, PC_out, out_A, out_B;

    registers32 write_instr_out(instr_out, instr_in, clock, enable, reset);
    registers32 write_PC_out(PC_out, PC_in, clock, enable, reset);
    registers32 write_out_A(out_A, in_A, clock, enable, reset);
    registers32 write_out_B(out_B, in_B, clock, enable, reset);

endmodule
