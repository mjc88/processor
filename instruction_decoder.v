module instruction_decoder(instruction, rs, rt, rd, SHAMT, opcode, alu_opcode, immediate,
target, flag_r, flag_i, flag_ji, flag_jii, ismult, isdiv, isWE);
    input[31:0] instruction;

    output[4:0] rs, rt, rd, SHAMT, opcode, alu_opcode;
    output[16:0] immediate;
    output[26:0] target;
    output flag_r, flag_i, flag_ji, flag_jii, ismult, isdiv, isWE;

    // following instruction machine code format from doc
    assign opcode = instruction[31:27];
    assign rd = instruction[26:22];
    assign rs = instruction[21:17];
    assign rt = instruction[16:12];
    assign SHAMT = instruction[11:7];
    assign alu_opcode = instruction[6:2];
    assign immediate = instruction[16:0];
    assign target = instruction[26:0];

    // determining instruction based on opcode
    wire op1, op2, op3, op4, op0;

    assign op4 = opcode[4];
    assign op3 = opcode[3];
    assign op2 = opcode[2];
    assign op1 = opcode[1];
    assign op0 = opcode[0];

    wire addi, basic_arith, sw, lw, jt, bne, jal_t, jr, blt, bex_t, setx_t;

    assign basic_arith = ~op4 & ~op3 & ~op2 & ~op1 & ~op0;
    assign addi = ~op4 & ~op3 & op2 & ~op1 & op0;
    assign sw = ~op4 & ~op3 & op2 & op1 & op0;
    assign lw = ~op4 & op3 & ~op2 & ~op1 & ~op0;
    assign jt = ~op4 & ~op3 & ~op2 & ~op1 & op0;
    assign bne = ~op4 & ~op3 & ~op2 & op1 & ~op0;
    assign jal_t = ~op4 & ~op3 & ~op2 & op1 & op0;
    assign jr = ~op4 & ~op3 & op2 & ~op1 & ~op0;
    assign blt = ~op4 & ~op3 & op2 & op1 & ~op0;
    assign bex_t = op4 & ~op3 & op2 & op1 & ~op0;
    assign setx_t = op4 & ~op3 & op2 & ~op1 & op0;

    // flag type of instruction - I, R, JI, JII
    assign flag_r = basic_arith;
    or assign_I_type(flag_i, addi, sw, lw, bne, blt);
    or assign_JI_type(flag_ji, bex_t, setx_t, jt, jal_t);
    assign flag_jii = jr;

    // alu opcode
    wire a1, a2, a3, a4, a0;

    assign a4 = alu_opcode[4];
    assign a3 = alu_opcode[3];
    assign a2 = alu_opcode[2];
    assign a1 = alu_opcode[1];
    assign a0 = alu_opcode[0];

    // alu or multdiv

    // writes and saves to regfile, turn on write enable 
    or assign_isWE(isWE, lw, addi, flag_r, jal_t, bex_t, setx_t);

endmodule



