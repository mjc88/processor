/**
 * READ THIS DESCRIPTION!
 *
 * This is your processor module that will contain the bulk of your code submission. You are to implement
 * a 5-stage pipelined processor in this module, accounting for hazards and implementing bypasses as
 * necessary.
 *
 * Ultimately, your processor will be tested by a master skeleton, so the
 * testbench can see which controls signal you active when. Therefore, there needs to be a way to
 * "inject" imem, dmem, and regfile interfaces from some external controller module. The skeleton
 * file, Wrapper.v, acts as a small wrapper around your processor for this purpose. Refer to Wrapper.v
 * for more details.
 *
 * As a result, this module will NOT contain the RegFile nor the memory modules. Study the inputs 
 * very carefully - the RegFile-related I/Os are merely signals to be sent to the RegFile instantiated
 * in your Wrapper module. This is the same for your memory elements. 
 *
 *
 */
module processor(
    // Control signals
    clock,                          // I: The master clock
    reset,                          // I: A reset signal

    // Imem
    address_imem,                   // O: The address of the data to get from imem
    q_imem,                         // I: The data from imem

    // Dmem
    address_dmem,                   // O: The address of the data to get or put from/to dmem
    data,                           // O: The data to write to dmem
    wren,                           // O: Write enable for dmem
    q_dmem,                         // I: The data from dmem

    // Regfile
    ctrl_writeEnable,               // O: Write enable for RegFile
    ctrl_writeReg,                  // O: Register to write to in RegFile
    ctrl_readRegA,                  // O: Register to read from port A of RegFile
    ctrl_readRegB,                  // O: Register to read from port B of RegFile
    data_writeReg,                  // O: Data to write to for RegFile
    data_readRegA,                  // I: Data from port A of RegFile
    data_readRegB                   // I: Data from port B of RegFile
	 
	);

	// Control signals
	input clock, reset;
	
	// Imem
    output [31:0] address_imem;     
	input [31:0] q_imem;            

	// Dmem
	output [31:0] address_dmem, data;
	output wren;        
	input [31:0] q_dmem;

	// Regfile
	output ctrl_writeEnable;
	output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	output [31:0] data_writeReg;
	input [31:0] data_readRegA, data_readRegB;

	/* YOUR CODE STARTS HERE */
    // MODULES
    // FETCH - control unit fetches the instruction's address from the memory unit
    // track pc and feed that into i-mem
    // q_imem output of ROM
    wire [31:0] PC_in, PC_out, PC_plus1, instr_out_fd, pc_out_fd;
    wire clock_rise, clock_fall;

    assign clock_rise = clock;
    assign clock_fall = ~clock;

    // use own adder
    assign PC_plus1 = PC_out + 32'b1;        // eventually will be a mux chain

    registers32 init_PC(PC_out, PC_plus1, clock, 1'b1, reset);  // change WE when want to stall

    // assign address imem to be the out PC
    assign address_imem = PC_out;
    // q_imem is the instruction and was updated
    FD_latch fd(instr_out_fd, pc_out_fd, q_imem, PC_out, clock, 1'b1, reset);

    //DECODE
    wire [4:0] rs, rt, rd, shamt, alu_op, opcode;
    wire [16:0] immediate;
    wire[26:0] target;
    wire flag_r, flag_i, flag_ji, flag_jii, ismult, isdiv, using_rd;

    //instruction_decoder decoding(instr_out_fd, rs, rt, rd, shamt, opcode, alu_op, immediate, target, flag_r, flag_i, flag_ji, flag_jii, ismult, isdiv);

    assign rd = instr_out_fd[26:22];
    assign rs = instr_out_fd[21:17];
    assign rt = instr_out_fd[16:12];

    // sw, jr, blt, bne use data within rd - ctrl_readRegB should be rd
    assign using_rd = (instr_out_fd[31:27]==5'b00111) | 
                      (instr_out_fd[31:27]==5'b00100) | 
                      (instr_out_fd[31:27]==5'b00110) |
                      (instr_out_fd[31:27]==5'b00010);
                
    
    //ctrl_writeReg is d (write back)
    // uncomment when ready. replace out_MW w a variable
    assign ctrl_writeReg = instr_out_mw;
    assign ctrl_readRegA = rs;
    assign ctrl_readRegB = using_rd ? rd : rt;

    // addi - destination reg, in reg, value
    wire[31:0] instr_out_dx, regfile_A, regfile_B, pc_out_dx, sign_extended_imm, alu_inputB, alu_result;
    wire using_sign_extended, is_not_equal, is_less_than, overflow;

    DX_latch dx(instr_out_dx, pc_out_dx, regfile_A, regfile_B, instr_out_fd, pc_out_fd, data_readRegA, data_readRegB, clock,1'b1, reset);
    

    // EXECUTE

    instruction_decoder getData(.instruction(instr_out_dx), .SHAMT(shamt), .opcode(opcode), .alu_opcode(alu_op), .immediate(immediate), .target(target), .flag_r(flag_r), .flag_i(flag_i), .flag_ji(flag_ji), .flag_jii(flag_jii), .ismult(ismult), .isdiv(isdiv));
    // immed, lw, sw - opcode like 98; data_opB will be the sign extended imm or regFileB
    assign using_sign_extended = (instr_out_dx[31:27]==5'b00111) | 
                      (instr_out_dx[31:27]==5'b01000) | 
                      (instr_out_dx[31:27]==5'b00101);

    sign_extender extend_immed(sign_extended_imm, immediate);
    assign alu_inputB = using_sign_extended ? sign_extended_imm : regfile_B;

    // mux for alu op
    assign alu_op = flag_r ? 5'b00000 : alu_op;
    
    alu alu_operating(regfile_A, alu_inputB, alu_op, shamt, alu_result, is_not_equal, is_less_than, overflow);

    wire[31:0] instr_out_xm, alu_out_xm, out_b_xm;

    XM_latch xm(instr_out_xm, alu_out_xm, out_b_xm, instr_out_dx, alu_result, regfile_B, clock, 1'b1, reset);

	assign address_dmem = alu_out_xm;
    assign data = out_b_xm;

    // WRITE BACK
    wire[31:0] instr_out_mw, alu_out_mw, data_out_mw;
    wire is_lw, isWE;
    assign is_lw = instr_out_fd[31:27]==5'b01000;
    // ASK WHAT THE SECOND INPUT IS
    MW_latch mw(instr_out_mw, alu_out_mw, data_out_mw, instr_out_xm, alu_out_xm, q_dmem, clock, 1'b1, reset);

    instruction_decoder getData2(.instruction(instr_out_mw), .isWE(isWE));
    assign ctrl_writeEnable = isWE;
    // mux after MW latch - CHECK THIS LOGIC
    assign data_writeReg = is_lw ? data_out_mw : alu_out_mw;
	/* END CODE */

endmodule
