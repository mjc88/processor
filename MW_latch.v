module MW_latch(instr_out, alu_out, data_out, instr_in, alu_in, data_in, clock, enable, reset);
    input [31:0] instr_in, alu_in, data_in;
    input clock, enable, reset;

    output[31:0] instr_out, alu_out, data_out;

    registers32 write_instr_out(instr_out, instr_in, clock, enable, reset);
    registers32 write_alu_out(alu_out, alu_in, clock, enable, reset);
    registers32 write_data_out(data_out, data_in, clock, enable, reset);

endmodule