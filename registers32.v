module registers32(out, in, clk, ctrl_writeEnable, ctrl_reset);
    input [31:0] in;
    input clk, ctrl_writeEnable, ctrl_reset;

    output [31:0] out;

    genvar regLoop;
    generate 
        for (regLoop = 0; regLoop <=31; regLoop = regLoop + 1)begin: loop
            dffe_ref dffe(out[regLoop], in[regLoop], clk, ctrl_writeEnable, ctrl_reset);
        end
    endgenerate

endmodule