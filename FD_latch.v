module FD_latch(instr_out, PC_out, instr_in, PC_in, clock, enable, reset);
    input [31:0] instr_in, PC_in;
    input clock, enable, reset;

    output[31:0] instr_out, PC_out;

    // this allows me to pass the instruction forward
    registers32 write_instr_out(instr_out, instr_in, clock, enable, reset);
    registers32 write_PC_out(PC_out, PC_in, clock, enable, reset);

endmodule
